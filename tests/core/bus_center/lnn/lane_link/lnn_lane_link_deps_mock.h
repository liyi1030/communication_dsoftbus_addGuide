/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LNN_LANE_LINK_DEPS_MOCK_H
#define LNN_LANE_LINK_DEPS_MOCK_H

#include <gmock/gmock.h>

#include "lnn_lane_assign.h"
#include "lnn_lane_link.h"
#include "lnn_lane_link_p2p.h"
#include "softbus_proxychannel_pipeline.h"
#include "wifi_direct_manager.h"

namespace OHOS {
class LaneLinkDepsInterface {
public:
    LaneLinkDepsInterface() {};
    virtual ~LaneLinkDepsInterface() {};

    virtual int32_t GetTransOptionByLaneReqId(uint32_t laneReqId, TransOption *reqInfo) = 0;
    virtual struct WifiDirectManager* GetWifiDirectManager(void) = 0;
    virtual int32_t TransProxyPipelineGenRequestId(void) = 0;
    virtual int32_t TransProxyPipelineOpenChannel(int32_t requestId, const char *networkId,
        const TransProxyPipelineChannelOption *option, const ITransProxyPipelineCallback *callback) = 0;
    virtual int32_t TransProxyPipelineCloseChannel(int32_t channelId) = 0;
    virtual int32_t TransProxyPipelineCloseChannelDelay(int32_t channelId) = 0;
};

class LaneLinkDepsInterfaceMock : public LaneLinkDepsInterface {
public:
    LaneLinkDepsInterfaceMock();
    ~LaneLinkDepsInterfaceMock() override;

    MOCK_METHOD2(GetTransOptionByLaneReqId, int32_t (uint32_t laneReqId, TransOption *reqInfo));
    MOCK_METHOD0(GetWifiDirectManager, struct WifiDirectManager* (void));
    MOCK_METHOD0(TransProxyPipelineGenRequestId, int32_t (void));
    MOCK_METHOD4(TransProxyPipelineOpenChannel, int32_t (int32_t requestId, const char *networkId,
        const TransProxyPipelineChannelOption *option, const ITransProxyPipelineCallback *callback));
    MOCK_METHOD1(TransProxyPipelineCloseChannel, int32_t (int32_t channelId));
    MOCK_METHOD1(TransProxyPipelineCloseChannelDelay, int32_t (int32_t channelId));
};
} // namespace OHOS
#endif // LNN_LANE_LINK_DEPS_MOCK_H
